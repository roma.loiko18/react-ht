import { idGenerator } from '../helpers/idGenerator';
import { dateGenerator } from '../helpers/dateGeneratop';

class Course {
	constructor(title = '', description = '', duration = 0, authors = []) {
		this.title = title;
		this.description = description;
		this.duration = duration;
		this.authors = [...authors];
		this.creationDate = dateGenerator();
		this.id = idGenerator();
	}

	setTitle(title) {
		this.title = title;
	}

	setDescription(description) {
		this.description = description;
	}

	setDuration(duration) {
		this.duration = duration;
	}

	setAuthors(authors) {
		this.authors = [...authors];
	}
}

export { Course };
