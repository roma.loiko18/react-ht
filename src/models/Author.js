import { idGenerator } from '../helpers/idGenerator';

class Author {
	constructor(name, id) {
		this.name = name;
		this.id = id || idGenerator();
	}
}

export { Author };
