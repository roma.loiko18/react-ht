const dateGenerator = () => {
	const now = Date.now();
	const today = new Date(now).toLocaleDateString();
	return today.replaceAll('/', '-');
};

export { dateGenerator };
