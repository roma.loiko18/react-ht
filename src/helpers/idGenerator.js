const idGenerator = () => {
	return (Math.random().toString(36) + '00000000000000000').slice(2, 9);
};

export { idGenerator };
