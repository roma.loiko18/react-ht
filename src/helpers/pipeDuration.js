const pipeDuration = (mins) => {
	let hours = Math.floor(mins / 60);
	let minutes = mins % 60;
	hours = hours < 10 ? '0' + hours : hours;
	minutes = minutes < 10 ? '0' + minutes : minutes;
	return `${hours}:${minutes}`;
};

export { pipeDuration };
