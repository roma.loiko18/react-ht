import React from 'react';
import { useSelector } from 'react-redux';
import { CourseCard } from './components/CourseCard/CourseCard';
import { getSearchedCourses } from '../../store/courses/selectors';

const Courses = () => {
	const searchedCourses = useSelector(getSearchedCourses);

	const coursesList = searchedCourses.map((course) => (
		<CourseCard
			key={course.id}
			id={course.id}
			title={course.title}
			description={course.description}
			authors={[...course.authors]}
			duration={course.duration}
			creationDate={course.creationDate}
		/>
	));

	return <div>{coursesList}</div>;
};

export { Courses };
