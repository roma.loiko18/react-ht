import React from 'react';

import './CourseCard.css';

import { Col, Container, Row } from 'react-bootstrap';
import { Button } from '../../../../common/Button/Button';
import { getAuthorsNameById } from '../../../../services/authorsService';
import { pipeDuration } from '../../../../helpers/pipeDuration';
import { useDispatch, useSelector } from 'react-redux';
import {
	setEditedCourse,
	showEditCourseModal,
} from '../../../../store/courses';
import { buttonText } from '../../../../constants/ButtonContent';
import { getAuthors } from '../../../../store/authors/selectors';

const CourseCard = (props) => {
	const dispatch = useDispatch();
	const authorsFromState = useSelector(getAuthors);
	const { title, description, authors, duration, creationDate, id } = props;
	const authorsList = authors
		.map((authorId) => getAuthorsNameById(authorId, authorsFromState))
		.join(', ');

	const showModal = () => {
		dispatch(setEditedCourse({ id }));
		dispatch(showEditCourseModal());
	};

	return (
		<Container className='card'>
			<Row className='d-flex align-items-center justify-content-between pt-2'>
				<Col xs={8} className='d-flex flex-md-column'>
					<h3>{title}</h3>
					<p>{description}</p>
				</Col>
				<Col xs={4} className='d-flex flex-md-column'>
					<ul>
						<li>
							<b>Authors:</b> {authorsList}
						</li>
						<li>
							<b>Duration:</b> {pipeDuration(duration)}
						</li>
						<li>
							<b>Created:</b> {creationDate}
						</li>
					</ul>
					<Button onClick={showModal}>{buttonText.SHOW_COURSE}</Button>
				</Col>
			</Row>
		</Container>
	);
};

export { CourseCard };
