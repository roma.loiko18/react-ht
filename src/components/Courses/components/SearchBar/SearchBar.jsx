import React, { useState } from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { Button } from '../../../../common/Button/Button';
import { useDispatch } from 'react-redux';
import { searchCourses } from '../../../../store/courses';
import { buttonText } from '../../../../constants/ButtonContent';

const SearchBar = () => {
	const [query, setQuery] = useState('');
	const dispatch = useDispatch();
	const searchHandler = () => {
		dispatch(searchCourses({ query }));
	};

	return (
		<Form>
			<Row>
				<Col>
					<Form.Control
						type='text'
						placeholder='Enter title'
						onChange={(e) => setQuery(e.target.value)}
					/>
				</Col>
				<Col>
					<Button onClick={searchHandler}>{buttonText.SEARCH}</Button>
				</Col>
			</Row>
		</Form>
	);
};

export { SearchBar };
