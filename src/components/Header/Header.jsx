import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Logo } from './components/Logo/Logo';
import { Button } from '../../common/Button/Button';
import { buttonText } from '../../constants/ButtonContent';

const Header = () => {
	return (
		<>
			<Container>
				<Row className='d-flex align-items-center pt-2'>
					<Col xs={10}>
						<Logo />
					</Col>
					<Col xs={1}>
						<h4>Name</h4>
					</Col>
					<Col xs={1}>
						<Button>{buttonText.LOGOUT}</Button>
					</Col>
				</Row>
			</Container>
			<hr />
		</>
	);
};

export default Header;
