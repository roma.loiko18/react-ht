import React from 'react';
import { Button } from '../../common/Button/Button';
import { useDispatch } from 'react-redux';
import { showCreateCourseModal } from '../../store/courses';
import { buttonText } from '../../constants/ButtonContent';

const CreateCourse = () => {
	const dispatch = useDispatch();

	const onShowModalHandler = () => {
		dispatch(showCreateCourseModal());
	};

	return (
		<Button onClick={onShowModalHandler}>{buttonText.CREATE_COURSE}</Button>
	);
};

export { CreateCourse };
