import React from 'react';
import { Container, ListGroup } from 'react-bootstrap';
import { AuthorCard } from './AuthorCard';

const Authors = ({ authors, add, clickHandler }) => {
	const authorsList = authors.map((author) => {
		return (
			<AuthorCard
				clickHandler={clickHandler}
				id={author.id}
				key={author.id}
				name={author.name}
				add={add}
			/>
		);
	});

	return (
		<div>
			<Container>
				<ListGroup>{authorsList}</ListGroup>
			</Container>
		</div>
	);
};

export { Authors };
