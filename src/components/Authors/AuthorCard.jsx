import React from 'react';
import { ListGroup } from 'react-bootstrap';
import { Button } from '../../common/Button/Button';
import { buttonText } from '../../constants/ButtonContent';
import PropTypes from 'prop-types';

const AuthorCard = ({ name, clickHandler, id, add }) => {
	const authorAction = () => {
		clickHandler({ id });
	};

	const text = add ? buttonText.ADD_AUTHOR : buttonText.REMOVE_AUTHOR;

	return (
		<ListGroup.Item className='d-flex justify-content-between'>
			<span>{name}</span>
			<Button onClick={authorAction}>{text}</Button>
		</ListGroup.Item>
	);
};

AuthorCard.propTypes = {
	name: PropTypes.string.isRequired,
	clickHandler: PropTypes.func.isRequired,
	id: PropTypes.string.isRequired,
	add: PropTypes.bool.isRequired,
};

export { AuthorCard };
