const inputPlaceholder = {
	ENTER_AUTHORS_NAME: 'Enter author`s name',
	ENTER_DURATION_IN_MINUTES: 'Enter duration of the course in minutes',
	ENTER_TITLE: 'Enter title',
	ENTER_DESCRIPTION: 'Enter description',
};

export { inputPlaceholder };
