const buttonText = {
	ADD_AUTHOR: 'Add author',
	REMOVE_AUTHOR: 'Remove author',
	SHOW_COURSE: 'Show Course',
	SEARCH: 'Search',
	CREATE_COURSE: 'Create Course',
	LOGOUT: 'Logout',
	CREATE_AUTHOR: 'Create author',
};

export { buttonText };
