const titleText = {
	CREATE_AUTHOR: 'Create Author',
	DURATION: 'Duration',
	DESCRIPTION: 'Description',
	TITLE: 'Title',
	AUTHORS: 'Authors',
	ADDED_AUTHORS: 'Added authors',
};

export { titleText };
