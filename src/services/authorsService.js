const getAuthorsNameById = (id, authors) => {
	const result = authors.reduce(
		(res, author) => (author.id === id ? { ...author } : res),
		{}
	);
	return result.name;
};

const getAuthorById = (id, authors) => {
	return authors.reduce(
		(res, author) => (author.id === id ? { ...author } : res),
		{}
	);
};

export { getAuthorsNameById, getAuthorById };
