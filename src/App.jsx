import React from 'react';
import Header from './components/Header/Header';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { CreateCourse } from './components/CreateCourse/CreateCourse';
import { Col, Container, Row } from 'react-bootstrap';
import { SearchBar } from './components/Courses/components/SearchBar/SearchBar';
import { Courses } from './components/Courses/Courses';
import { AddNewCourseModal } from './modals/AddNewCourse/AddNewCourse';
import { EditCourseModal } from './modals/EditCourse/EditCourse';

const App = () => {
	return (
		<>
			<EditCourseModal />
			<AddNewCourseModal />
			<Header />
			<Container>
				<Row>
					<Col xs={10}>
						<SearchBar />
					</Col>
					<Col xs={2}>
						<CreateCourse />
					</Col>
				</Row>
			</Container>
			<hr />
			<Courses />
		</>
	);
};

export { App };
