import React from 'react';
import { Col, Modal, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { hideModal } from '../../store/courses';
import { ModalHeader } from './components/ModalHeader';
import { ModalCreateAuthor } from './components/ModalCreateAuthor';
import { ModalDuration } from './components/ModalDuration';
import { Authors } from '../../components/Authors/Authors';
import { addAuthorToCourse, removeAuthorFromCourse } from '../../store/authors';
import { getAuthorsState } from '../../store/authors/selectors';
import { getShowCreateCourseModal } from '../../store/courses/selectors';
import { titleText } from '../../constants/Titles';

const AddNewCourseModal = () => {
	const { availableToAddToCourseAuthors, addedToCourseAuthors } =
		useSelector(getAuthorsState);
	const show = useSelector(getShowCreateCourseModal);

	const dispatch = useDispatch();

	const addAuthorHandler = ({ id }) => {
		dispatch(addAuthorToCourse({ id }));
	};

	const removeAuthorHandler = ({ id }) => {
		dispatch(removeAuthorFromCourse({ id }));
	};

	return (
		<Modal size='lg' show={show} onHide={() => dispatch(hideModal())}>
			<Modal.Header>
				<Modal.Title>Create new course</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<ModalHeader />
				<Row>
					<Col xs={6}>
						<ModalCreateAuthor />
						<ModalDuration />
					</Col>
					<Col xs={6}>
						<span>{titleText.AUTHORS}</span>
						<Authors
							clickHandler={addAuthorHandler}
							authors={availableToAddToCourseAuthors}
							add={true}
						/>
						<hr />
						<span>{titleText.ADDED_AUTHORS}</span>
						<Authors
							clickHandler={removeAuthorHandler}
							authors={addedToCourseAuthors}
							add={false}
						/>
					</Col>
				</Row>
			</Modal.Body>
		</Modal>
	);
};

export { AddNewCourseModal };
