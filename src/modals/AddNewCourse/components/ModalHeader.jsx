import React from 'react';
import { Col, Form, Row } from 'react-bootstrap';
import { Button } from '../../../common/Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import {
	createCourse,
	setAuthors,
	setDescription,
	setTitle,
} from '../../../store/courses';
import { setDefault } from '../../../store/authors';
import { getAddedToCourseAuthors } from '../../../store/authors/selectors';
import { inputPlaceholder } from '../../../constants/InputPlaceholder';
import { buttonText } from '../../../constants/ButtonContent';
import { titleText } from '../../../constants/Titles';

const ModalHeader = () => {
	const dispatch = useDispatch();
	const addedAuthors = useSelector(getAddedToCourseAuthors);

	const createCourseHandler = () => {
		const addedAuthorsIdArray = addedAuthors.map((author) => author.id);
		dispatch(setAuthors({ addedAuthorsIdArray }));
		dispatch(createCourse());
		dispatch(setDefault());
	};

	const setTitleHandler = (title) => {
		dispatch(setTitle({ title }));
	};

	const setDescriptionHandler = (description) => {
		dispatch(setDescription({ description }));
	};

	return (
		<>
			<Row className='d-flex justify-content-between align-items-end'>
				<Col xs={6}>
					<Form>
						<Form.Label>{titleText.TITLE}</Form.Label>
						<Form.Control
							placeholder={inputPlaceholder.ENTER_TITLE}
							onChange={(e) => setTitleHandler(e.target.value)}
							required
						/>
					</Form>
				</Col>

				<Col xs={3}>
					<Button onClick={createCourseHandler}>
						{buttonText.CREATE_COURSE}
					</Button>
				</Col>

				<Form>
					<Form.Group className='mb-3' controlId='exampleForm.ControlTextarea1'>
						<Form.Label>{titleText.DESCRIPTION}</Form.Label>
						<Form.Control
							as='textarea'
							rows={3}
							placeholder={inputPlaceholder.ENTER_DESCRIPTION}
							onChange={(e) => setDescriptionHandler(e.target.value)}
							required
						/>
					</Form.Group>
				</Form>
			</Row>
			<hr />
		</>
	);
};

export { ModalHeader };
