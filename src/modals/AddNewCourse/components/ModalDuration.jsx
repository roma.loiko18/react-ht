import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import { pipeDuration } from '../../../helpers/pipeDuration';
import { useDispatch } from 'react-redux';
import { setDuration } from '../../../store/courses';
import { inputPlaceholder } from '../../../constants/InputPlaceholder';
import { titleText } from '../../../constants/Titles';

const ModalDuration = () => {
	const [duration, setThisDuration] = useState(0);
	const dispatch = useDispatch();

	const setDurations = (e) => {
		setThisDuration(e.target.value);
		dispatch(setDuration({ duration: e.target.value }));
	};

	return (
		<Form>
			<Form.Label>{titleText.DURATION}</Form.Label>
			<Form.Control
				placeholder={inputPlaceholder.ENTER_DURATION_IN_MINUTES}
				className='mb-2'
				type='number'
				value={duration}
				onChange={(e) => setDurations(e)}
				required
			/>
			<h5>Duration: {pipeDuration(parseInt(duration))} hours</h5>
		</Form>
	);
};

export { ModalDuration };
