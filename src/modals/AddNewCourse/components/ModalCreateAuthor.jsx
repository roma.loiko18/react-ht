import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import { Button } from '../../../common/Button/Button';
import { useDispatch } from 'react-redux';
import { createAuthor } from '../../../store/authors';
import { inputPlaceholder } from '../../../constants/InputPlaceholder';
import { buttonText } from '../../../constants/ButtonContent';
import { titleText } from '../../../constants/Titles';

const ModalCreateAuthor = () => {
	const dispatch = useDispatch();
	const [name, setName] = useState('');
	const createAuthorHandler = () => {
		dispatch(createAuthor({ name }));
		setName('');
	};

	return (
		<Form className='mb-5'>
			<Form.Label>{titleText.CREATE_AUTHOR}</Form.Label>
			<Form.Control
				placeholder={inputPlaceholder.ENTER_AUTHORS_NAME}
				className='mb-2'
				value={name}
				onChange={(e) => setName(e.target.value)}
			/>
			<Button onClick={createAuthorHandler}>{buttonText.CREATE_AUTHOR}</Button>
		</Form>
	);
};

export { ModalCreateAuthor };
