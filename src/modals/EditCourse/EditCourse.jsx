import React, { useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Modal } from 'react-bootstrap';
import { deleteCourse, hideModal, saveChanges } from '../../store/courses';
import { pipeDuration } from '../../helpers/pipeDuration';
import { Button } from '../../common/Button/Button';
import {
	getEditedCourse,
	getShowEditCourseModal,
} from '../../store/courses/selectors';

const EditCourseModal = () => {
	const dispatch = useDispatch();
	const show = useSelector(getShowEditCourseModal);
	const { title, duration } = useSelector(getEditedCourse);

	const descriptionRef = useRef();

	const saveChangesHandler = () => {
		dispatch(saveChanges({ description: descriptionRef.current.value }));
		dispatch(hideModal());
	};

	const deleteCourseHandler = () => {
		dispatch(deleteCourse());
		dispatch(hideModal());
	};

	const hide = () => {
		dispatch(hideModal());
	};

	return (
		<Modal size='lg' show={show} onHide={() => dispatch(hideModal())}>
			<Modal.Header>
				<Modal.Title>Edit Course</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form>
					<Form.Label>Title</Form.Label>
					<Form.Control
						placeholder='Enter duration of the course in minutes'
						className='mb-2'
						value={title}
						inputRef={(ref) => {
							this.input = ref;
						}}
						required
					/>
				</Form>
				<Form>
					<Form.Label>Duration</Form.Label>
					<Form.Control
						placeholder='Enter duration of the course in minutes'
						className='mb-2'
						value={duration}
						required
					/>
					<h5>Duration: {pipeDuration(parseInt(duration))} hours</h5>
				</Form>

				<Form>
					<Form.Label>Description</Form.Label>
					<Form.Control
						as='textarea'
						rows={4}
						placeholder='Write description'
						className='mb-2'
						ref={descriptionRef}
						required
					/>
				</Form>
				<Modal.Footer>
					<Button onClick={hide}>Cancel</Button>
					<Button onClick={deleteCourseHandler} warn={true}>
						Delete Course
					</Button>
					<Button onClick={saveChangesHandler}>Save</Button>
				</Modal.Footer>
			</Modal.Body>
		</Modal>
	);
};

export { EditCourseModal };
