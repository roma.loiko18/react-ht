import React from 'react';
import { Button as BootstrapButton } from 'react-bootstrap';

const Button = (props) => {
	const { children, onClick, warn } = props;
	const variant = warn ? 'outline-danger' : 'outline-primary';

	return (
		<BootstrapButton variant={variant} onClick={onClick}>
			{children}
		</BootstrapButton>
	);
};

export { Button };
