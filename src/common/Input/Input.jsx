import React from 'react';

const Input = (props) => {
	const { text } = props;
	return <input type='text' placeholder={text} />;
};

export { Input };
