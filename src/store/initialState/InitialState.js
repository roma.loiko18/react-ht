import { Course } from '../../models/Course';
import { Author } from '../../models/Author';

const initialCoursesState = [
	new Course(
		'JavaScript',
		`Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the
1500s, when an unknown  printer took a galley of type and scrambled it to make a type
specimen book. It has survived  not only five centuries, but also the leap into electronic
COMPONENTS.md 1/4/20223 / 11 typesetting, remaining essentially u  nchanged.`,
		160,
		[
			'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			'f762978b-61eb-4096-812b-ebde22838167',
		]
	),

	new Course(
		'Angular',
		`Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum  has been the industry's standard dummy text ever since the
1500s, when an unknown  printer took a galley of type and scrambled it to make a type specimen book.`,
		210,
		[
			'df32994e-b23d-497c-9e4d-84e4dc02882f',
			'095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		]
	),
];

const initialAuthorsState = [
	new Author('Vasiliy Dobkin', '27cc3006-e93a-4748-8ca8-73d06aa93b6d'),
	new Author('Nicolas Kim', 'f762978b-61eb-4096-812b-ebde22838167'),
	new Author('Anna Sidorenko', 'df32994e-b23d-497c-9e4d-84e4dc02882f'),
	new Author('Valentina Larina', '095a1817-d45b-4ed7-9cf7-b2417bcbf748'),
];

export { initialCoursesState, initialAuthorsState };
