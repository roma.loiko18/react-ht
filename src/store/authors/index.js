import { initialAuthorsState } from '../initialState/InitialState';
import { createSlice } from '@reduxjs/toolkit';
import { Author } from '../../models/Author';
import { getAuthorById } from '../../services/authorsService';

const initialState = {
	authors: initialAuthorsState,
	availableToAddToCourseAuthors: initialAuthorsState,
	addedToCourseAuthors: [],
};

export const authorsSlice = createSlice({
	name: 'authors',
	initialState,
	reducers: {
		createAuthor: (state, { payload }) => {
			const newAuthor = new Author(payload.name);
			state.authors = [...state.authors, newAuthor];
			state.availableToAddToCourseAuthors = [
				...state.availableToAddToCourseAuthors,
				newAuthor,
			];
		},
		addAuthorToCourse: (state, { payload }) => {
			const addedAuthor = getAuthorById(payload.id, state.authors);
			state.availableToAddToCourseAuthors = [
				...state.availableToAddToCourseAuthors,
			].filter((author) => author.id !== addedAuthor.id);
			state.addedToCourseAuthors = [...state.addedToCourseAuthors, addedAuthor];
		},
		removeAuthorFromCourse: (state, { payload }) => {
			const removedAuthor = getAuthorById(payload.id, state.authors);
			state.availableToAddToCourseAuthors = [
				...state.availableToAddToCourseAuthors,
				removedAuthor,
			];
			state.addedToCourseAuthors = [...state.addedToCourseAuthors].filter(
				(author) => author.id !== payload.id
			);
		},
		setDefault: (state) => {
			state.availableToAddToCourseAuthors = state.authors;
			state.addedToCourseAuthors = [];
		},
	},
});

export const {
	createAuthor,
	addAuthorToCourse,
	removeAuthorFromCourse,
	setDefault,
} = authorsSlice.actions;

export default authorsSlice.reducer;
