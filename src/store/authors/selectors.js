const getAuthorsState = (state) => state.authors;
const getAuthors = (state) => state.authors.authors;
const getAddedToCourseAuthors = (state) => state.authors.addedToCourseAuthors;
const getAvailableToAddToCourseAuthors = (state) =>
	state.authors.availableToAddToCourseAuthors;

export {
	getAvailableToAddToCourseAuthors,
	getAddedToCourseAuthors,
	getAuthorsState,
	getAuthors,
};
