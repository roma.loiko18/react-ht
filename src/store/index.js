import { configureStore } from '@reduxjs/toolkit';
import authorsStore from './authors';
import coursesStore from './courses';

export const store = configureStore({
	reducer: {
		authors: authorsStore,
		courses: coursesStore,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});
