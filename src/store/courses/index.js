import { initialCoursesState } from '../initialState/InitialState';
import { createSlice } from '@reduxjs/toolkit';
import { Course } from '../../models/Course';

const initialState = {
	courses: initialCoursesState,
	searchedCourses: initialCoursesState,
	newCourse: new Course(),
	editedCourse: new Course(),
	showCreateCourseModal: false,
	showEditCourseModal: false,
};

export const coursesSlice = createSlice({
	name: 'courses',
	initialState,
	reducers: {
		searchCourses: (state, { payload }) => {
			state.searchedCourses = [...state.courses].filter((course) =>
				course.title.includes(payload.query)
			);
		},
		createCourse: (state) => {
			const { title, description, duration, authors } = state.newCourse;
			const newCourse = new Course(title, description, duration, authors);
			state.courses = [...state.courses, newCourse];
			state.searchedCourses = state.courses;
			state.showCreateCourseModal = false;
		},
		setEditedCourse: (state, { payload }) => {
			state.editedCourse = [...state.courses].filter(
				(course) => course.id === payload.id
			)[0];
		},
		saveChanges: (state, { payload }) => {
			const index = state.courses.indexOf(state.editedCourse);
			const { description } = payload;
			state.courses[index] = { ...state.courses[index], description };
			state.searchedCourses = state.courses;
		},
		deleteCourse: (state) => {
			state.courses = state.courses.filter(
				(course) => course.id !== state.editedCourse.id
			);
			state.searchedCourses = state.courses;
		},
		setDescription: (state, { payload }) => {
			state.newCourse.setDescription(payload.description);
		},
		setTitle: (state, { payload }) => {
			state.newCourse.setTitle(payload.title);
		},
		setAuthors: (state, { payload }) => {
			state.newCourse.setAuthors(payload.addedAuthorsIdArray);
		},
		setDuration: (state, { payload }) => {
			state.newCourse.setDuration(payload.duration);
		},
		showCreateCourseModal: (state) => {
			state.showCreateCourseModal = true;
		},
		showEditCourseModal: (state) => {
			state.showEditCourseModal = true;
		},
		hideModal: (state) => {
			state.showCreateCourseModal = false;
			state.showEditCourseModal = false;
		},
	},
});

export const {
	showCreateCourseModal,
	hideModal,
	createCourse,
	setDuration,
	setTitle,
	setAuthors,
	setDescription,
	searchCourses,
	setEditedCourse,
	showEditCourseModal,
	saveChanges,
	deleteCourse,
} = coursesSlice.actions;
export default coursesSlice.reducer;
