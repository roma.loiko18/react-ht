const getCourses = (state) => state.courses.courses;
const getSearchedCourses = (state) => state.courses.searchedCourses;
const getNewCourse = (state) => state.courses.newCourse;
const getEditedCourse = (state) => state.courses.editedCourse;
const getShowCreateCourseModal = (state) => state.courses.showCreateCourseModal;
const getShowEditCourseModal = (state) => state.courses.showEditCourseModal;

export {
	getShowEditCourseModal,
	getShowCreateCourseModal,
	getNewCourse,
	getSearchedCourses,
	getCourses,
	getEditedCourse,
};
